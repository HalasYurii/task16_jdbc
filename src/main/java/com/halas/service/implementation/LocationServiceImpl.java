package com.halas.service.implementation;

import com.halas.dao.implementation.LocationDaoImpl;
import com.halas.model.Location;
import com.halas.service.interfaces.concrete.LocationService;

import java.sql.SQLException;
import java.util.List;

public class LocationServiceImpl implements LocationService {
    @Override
    public List<Location> findAll() throws SQLException {
        return new LocationDaoImpl().findAll();
    }

    @Override
    public Location findById(Integer id) throws SQLException {
        return new LocationDaoImpl().findById(id);
    }

    @Override
    public List<Location> findByCountry(String country) throws SQLException {
        return new LocationDaoImpl().findByCountry(country);
    }

    @Override
    public List<Location> findByAddress(String address) throws SQLException {
        return new LocationDaoImpl().findByAddress(address);
    }

    @Override
    public List<Location> findByPublishingHouseID(Integer id) throws SQLException {
        return new LocationDaoImpl().findByPublishingHouseID(id);
    }

    @Override
    public int create(Location entity) throws SQLException {
        return new LocationDaoImpl().create(entity);
    }

    @Override
    public int update(Location entity) throws SQLException {
        return new LocationDaoImpl().update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return new LocationDaoImpl().delete(id);
    }

    @Override
    public int deleteByCountry(String country) throws SQLException {
        return new LocationDaoImpl().deleteByCountry(country);
    }

    @Override
    public int deleteByAddress(String address) throws SQLException {
        return new LocationDaoImpl().deleteByAddress(address);
    }

    @Override
    public int deleteByPublishingHouseID(Integer id) throws SQLException {
        return new LocationDaoImpl().deleteByPublishingHouseID(id);
    }
}