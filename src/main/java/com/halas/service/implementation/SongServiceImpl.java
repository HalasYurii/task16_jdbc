package com.halas.service.implementation;

import com.halas.dao.implementation.SongDaoImpl;
import com.halas.model.Song;
import com.halas.service.interfaces.concrete.SongService;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class SongServiceImpl implements SongService {
    @Override
    public List<Song> findAll() throws SQLException {
        return new SongDaoImpl().findAll();
    }

    @Override
    public Song findById(Integer id) throws SQLException {
        return new SongDaoImpl().findById(id);
    }

    @Override
    public List<Song> findByName(String name) throws SQLException {
        return new SongDaoImpl().findByName(name);
    }

    @Override
    public List<Song> findByDateOut(Date date) throws SQLException {
        return new SongDaoImpl().findByDateOut(date);
    }

    @Override
    public List<Song> findByPerformerID(Integer id) throws SQLException {
        return new SongDaoImpl().findByPerformerID(id);
    }

    @Override
    public int create(Song entity) throws SQLException {
        return new SongDaoImpl().create(entity);
    }

    @Override
    public int update(Song entity) throws SQLException {
        return new SongDaoImpl().update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return new SongDaoImpl().delete(id);
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        return new SongDaoImpl().deleteByName(name);
    }

    @Override
    public int deleteByDateOut(Date date) throws SQLException {
        return new SongDaoImpl().deleteByDateOut(date);
    }

    @Override
    public int deleteByPerformerID(Integer id) throws SQLException {
        return new SongDaoImpl().deleteByPerformerID(id);
    }
}