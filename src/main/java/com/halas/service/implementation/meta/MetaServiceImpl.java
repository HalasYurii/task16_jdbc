package com.halas.service.implementation.meta;

import com.halas.dao.implementation.meta.MetaDaoImpl;
import com.halas.model.meta.TableMeta;
import com.halas.service.interfaces.concrete.meta.MetaService;

import java.sql.SQLException;
import java.util.List;

public class MetaServiceImpl implements MetaService {
    @Override
    public List<String> findAllTableName() throws SQLException {
        return new MetaDaoImpl().findAllTableName();
    }

    @Override
    public List<TableMeta> getTablesStructure() throws SQLException {
        return new MetaDaoImpl().getTablesStructure();
    }
}
