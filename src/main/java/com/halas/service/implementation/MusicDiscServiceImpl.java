package com.halas.service.implementation;

import com.halas.dao.implementation.MusicDiscDaoImpl;
import com.halas.model.MusicDisc;
import com.halas.service.interfaces.concrete.MusicDiscService;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class MusicDiscServiceImpl implements MusicDiscService {
    @Override
    public List<MusicDisc> findAll() throws SQLException {
        return new MusicDiscDaoImpl().findAll();
    }

    @Override
    public MusicDisc findById(Integer id) throws SQLException {
        return new MusicDiscDaoImpl().findById(id);
    }

    @Override
    public List<MusicDisc> findByName(String name) throws SQLException {
        return new MusicDiscDaoImpl().findByName(name);
    }

    @Override
    public List<MusicDisc> findByDateOut(Date date) throws SQLException {
        return new MusicDiscDaoImpl().findByDateOut(date);
    }

    @Override
    public List<MusicDisc> findByPublishingHouseID(Integer id) throws SQLException {
        return new MusicDiscDaoImpl().findByPublishingHouseID(id);
    }

    @Override
    public List<MusicDisc> findByType(String type) throws SQLException {
        return new MusicDiscDaoImpl().findByType(type);
    }

    @Override
    public int create(MusicDisc entity) throws SQLException {
        return new MusicDiscDaoImpl().create(entity);
    }

    @Override
    public int update(MusicDisc entity) throws SQLException {
        return new MusicDiscDaoImpl().update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return new MusicDiscDaoImpl().delete(id);
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        return new MusicDiscDaoImpl().deleteByName(name);
    }

    @Override
    public int deleteByType(String type) throws SQLException {
        return new MusicDiscDaoImpl().deleteByType(type);
    }

    @Override
    public int deleteByDateOut(Date date) throws SQLException {
        return new MusicDiscDaoImpl().deleteByDateOut(date);
    }

    @Override
    public int deleteByPublishingHouseID(Integer id) throws SQLException {
        return new MusicDiscDaoImpl().deleteByPublishingHouseID(id);
    }
}
