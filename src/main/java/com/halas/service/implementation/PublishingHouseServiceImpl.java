package com.halas.service.implementation;

import com.halas.dao.implementation.PublishingHouseDaoImpl;
import com.halas.model.PublishingHouse;
import com.halas.service.interfaces.concrete.PublishingHouseService;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class PublishingHouseServiceImpl implements PublishingHouseService {
    @Override
    public List<PublishingHouse> findAll() throws SQLException {
        return new PublishingHouseDaoImpl().findAll();
    }

    @Override
    public PublishingHouse findById(Integer id) throws SQLException {
        return new PublishingHouseDaoImpl().findById(id);
    }

    @Override
    public List<PublishingHouse> findByName(String name) throws SQLException {
        return new PublishingHouseDaoImpl().findByName(name);
    }

    @Override
    public List<PublishingHouse> findByDateBorn(Date date) throws SQLException {
        return new PublishingHouseDaoImpl().findByDateBorn(date);
    }

    @Override
    public int create(PublishingHouse entity) throws SQLException {
        return new PublishingHouseDaoImpl().create(entity);
    }

    @Override
    public int update(PublishingHouse entity) throws SQLException {
        return new PublishingHouseDaoImpl().update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return new PublishingHouseDaoImpl().delete(id);
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        return new PublishingHouseDaoImpl().deleteByName(name);
    }

    @Override
    public int deleteByDateBorn(Date date) throws SQLException {
        return new PublishingHouseDaoImpl().deleteByDateBorn(date);
    }
}