package com.halas.service.implementation;

import com.halas.dao.implementation.PerformerDaoImpl;
import com.halas.model.Performer;
import com.halas.service.interfaces.concrete.PerformerService;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class PerformerServiceImpl implements PerformerService {
    @Override
    public List<Performer> findAll() throws SQLException {
        return new PerformerDaoImpl().findAll();
    }

    @Override
    public Performer findById(Integer id) throws SQLException {
        return new PerformerDaoImpl().findById(id);
    }

    @Override
    public List<Performer> findByName(String name) throws SQLException {
        return new PerformerDaoImpl().findByName(name);
    }

    @Override
    public List<Performer> findBySurname(String surname) throws SQLException {
        return new PerformerDaoImpl().findBySurname(surname);
    }

    @Override
    public List<Performer> findByDateBorn(Date date) throws SQLException {
        return new PerformerDaoImpl().findByDateBorn(date);
    }

    @Override
    public List<Performer> findByMusicDiscID(Integer id) throws SQLException {
        return new PerformerDaoImpl().findByMusicDiscID(id);
    }

    @Override
    public int create(Performer entity) throws SQLException {
        return new PerformerDaoImpl().create(entity);
    }

    @Override
    public int update(Performer entity) throws SQLException {
        return new PerformerDaoImpl().update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return new PerformerDaoImpl().delete(id);
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        return new PerformerDaoImpl().deleteByName(name);
    }

    @Override
    public int deleteBySurname(String surname) throws SQLException {
        return new PerformerDaoImpl().deleteBySurname(surname);
    }

    @Override
    public int deleteByDateBorn(Date date) throws SQLException {
        return new PerformerDaoImpl().deleteByDateBorn(date);
    }

    @Override
    public int deleteByMusicDiscID(Integer id) throws SQLException {
        return new PerformerDaoImpl().deleteByMusicDiscID(id);
    }
}
