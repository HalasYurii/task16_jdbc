package com.halas.service.interfaces.find;

import com.halas.model.PublishingHouse;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface PublishingHouseFindService {
    List<PublishingHouse> findByName(String name) throws SQLException;

    List<PublishingHouse> findByDateBorn(Date date) throws SQLException;
}
