package com.halas.service.interfaces.find;

import com.halas.model.Location;

import java.sql.SQLException;
import java.util.List;

public interface LocationFindService {
    List<Location> findByCountry(String country) throws SQLException;

    List<Location> findByAddress(String address) throws SQLException;

    List<Location> findByPublishingHouseID(Integer id) throws SQLException;
}
