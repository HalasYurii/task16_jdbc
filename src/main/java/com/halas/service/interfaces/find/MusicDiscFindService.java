package com.halas.service.interfaces.find;

import com.halas.model.MusicDisc;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface MusicDiscFindService {
    List<MusicDisc> findByName(String name) throws SQLException;

    List<MusicDisc> findByType(String type) throws SQLException;

    List<MusicDisc> findByDateOut(Date date) throws SQLException;

    List<MusicDisc> findByPublishingHouseID(Integer id) throws SQLException;
}
