package com.halas.service.interfaces.concrete;

import com.halas.model.PublishingHouse;
import com.halas.service.interfaces.GeneralService;
import com.halas.service.interfaces.delete.PublishingHouseDeleteService;
import com.halas.service.interfaces.find.PublishingHouseFindService;

public interface PublishingHouseService extends
        GeneralService<PublishingHouse, Integer>,
        PublishingHouseFindService,
        PublishingHouseDeleteService {
}
