package com.halas.service.interfaces.concrete.meta;

import com.halas.model.meta.TableMeta;

import java.sql.SQLException;
import java.util.List;

public interface MetaService {
    List<String> findAllTableName() throws SQLException;

    List<TableMeta> getTablesStructure() throws SQLException;
}
