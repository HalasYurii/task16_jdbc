package com.halas.service.interfaces.concrete;

import com.halas.model.Song;
import com.halas.service.interfaces.GeneralService;
import com.halas.service.interfaces.delete.SongDeleteService;
import com.halas.service.interfaces.find.SongFindService;

public interface SongService extends
        GeneralService<Song, Integer>,
        SongFindService,
        SongDeleteService {
}
