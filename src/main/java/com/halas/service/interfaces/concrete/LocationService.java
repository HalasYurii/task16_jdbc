package com.halas.service.interfaces.concrete;

import com.halas.model.Location;
import com.halas.service.interfaces.GeneralService;
import com.halas.service.interfaces.delete.LocationDeleteService;
import com.halas.service.interfaces.find.LocationFindService;

public interface LocationService extends
        GeneralService<Location, Integer>,
        LocationFindService,
        LocationDeleteService {
}
