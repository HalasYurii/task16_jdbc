package com.halas.service.interfaces.concrete;

import com.halas.model.Performer;
import com.halas.service.interfaces.GeneralService;
import com.halas.service.interfaces.delete.PerformerDeleteService;
import com.halas.service.interfaces.find.PerformerFindService;

public interface PerformerService extends
        GeneralService<Performer, Integer>,
        PerformerFindService,
        PerformerDeleteService {
}
