package com.halas.service.interfaces.concrete;

import com.halas.model.MusicDisc;
import com.halas.service.interfaces.GeneralService;
import com.halas.service.interfaces.delete.MusicDiscDeleteService;
import com.halas.service.interfaces.find.MusicDiscFindService;

public interface MusicDiscService extends
        GeneralService<MusicDisc, Integer>,
        MusicDiscFindService,
        MusicDiscDeleteService {
}
