package com.halas.service.interfaces.delete;

import java.sql.SQLException;

public interface LocationDeleteService {
    int deleteByCountry(String country) throws SQLException;

    int deleteByAddress(String address) throws SQLException;

    int deleteByPublishingHouseID(Integer id) throws SQLException;
}
