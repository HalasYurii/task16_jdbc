package com.halas.service.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface SongDeleteService {
    int deleteByName(String name) throws SQLException;

    int deleteByDateOut(Date date) throws SQLException;

    int deleteByPerformerID(Integer id) throws SQLException;
}
