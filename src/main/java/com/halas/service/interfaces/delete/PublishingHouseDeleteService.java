package com.halas.service.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface PublishingHouseDeleteService {
    int deleteByName(String name) throws SQLException;

    int deleteByDateBorn(Date date) throws SQLException;
}
