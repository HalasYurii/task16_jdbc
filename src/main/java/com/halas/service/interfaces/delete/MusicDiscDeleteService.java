package com.halas.service.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface MusicDiscDeleteService {
    int deleteByName(String name) throws SQLException;

    int deleteByType(String type) throws SQLException;

    int deleteByDateOut(Date date) throws SQLException;

    int deleteByPublishingHouseID(Integer id) throws SQLException;
}
