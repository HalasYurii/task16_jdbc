package com.halas.view.realisation.submenu.delete;

import com.halas.controller.implementation.MusicDiscControllerImpl;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MenuDeleteMusicDisc implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private MusicDiscControllerImpl musicDiscController;
    private Logger log;

    public MenuDeleteMusicDisc() {
        musicDiscController = new MusicDiscControllerImpl();
        log = LogManager.getLogger(MenuDeleteMusicDisc.class);
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Delete data by id.");
        menuDescription.put("2", " 2 - Delete data by name.");
        menuDescription.put("3", " 3 - Delete data by type disc.");
        menuDescription.put("4", " 4 - Delete data by date out.");
        menuDescription.put("5", " 5 - Delete data by publishing house id.");
        menuDescription.put(BACK_POINT, " " + BACK_POINT + " - Back to music disc menu.");
    }

    private void initMenuMethods() {
        menuMethods.put("1", musicDiscController::delete);
        menuMethods.put("2", musicDiscController::deleteByName);
        menuMethods.put("3", musicDiscController::deleteByType);
        menuMethods.put("4", musicDiscController::deleteByDateOut);
        menuMethods.put("5", musicDiscController::deleteByPublishingHouseID);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(MENU_DELETE_BY_MUSIC_DISC);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(BACK_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}
