package com.halas.view.realisation.submenu.concrete;

import com.halas.controller.implementation.MusicDiscControllerImpl;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import com.halas.view.realisation.submenu.delete.MenuDeleteMusicDisc;
import com.halas.view.realisation.submenu.find.MenuFindMusicDisc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MenuMusicDisc implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private MusicDiscControllerImpl musicDiscController;
    private Logger log;

    public MenuMusicDisc() {
        musicDiscController = new MusicDiscControllerImpl();
        log = LogManager.getLogger(MenuMusicDisc.class);
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Show all data.");
        menuDescription.put("2", " 2 - Show menu \'find by\'.");
        menuDescription.put("3", " 3 - Create data in current table.");
        menuDescription.put("4", " 4 - Update data in current table.");
        menuDescription.put("5", " 5 - Show menu \'delete by\'.");
        menuDescription.put(BACK_POINT, " " + BACK_POINT + " - Back to music disc menu.");
    }

    private void initMenuMethods() {
        menuMethods.put("1", musicDiscController::showAll);
        menuMethods.put("2", new MenuFindMusicDisc()::run);
        menuMethods.put("3", musicDiscController::create);
        menuMethods.put("4", musicDiscController::update);
        menuMethods.put("5", new MenuDeleteMusicDisc()::run);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(MENU_MUSIC_DISC);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(BACK_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}
