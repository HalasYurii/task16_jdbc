package com.halas.view.realisation.submenu.delete;

import com.halas.controller.implementation.PublishingHouseControllerImpl;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MenuDeletePublishingHouse implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private PublishingHouseControllerImpl publishingHouseController;
    private Logger log;

    public MenuDeletePublishingHouse() {
        publishingHouseController = new PublishingHouseControllerImpl();
        log = LogManager.getLogger(MenuDeletePublishingHouse.class);
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Delete data by id.");
        menuDescription.put("2", " 2 - Delete data by name.");
        menuDescription.put("3", " 3 - Delete data by date born.");
        menuDescription.put(BACK_POINT, " " + BACK_POINT + " - Back to publishing house menu.");
    }

    private void initMenuMethods() {
        menuMethods.put("1", publishingHouseController::delete);
        menuMethods.put("2", publishingHouseController::deleteByName);
        menuMethods.put("3", publishingHouseController::deleteByDateBorn);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(MENU_DELETE_BY_PUBLISHING_HOUSE);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(BACK_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}
