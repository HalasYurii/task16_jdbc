package com.halas.view.realisation.submenu.delete;

import com.halas.controller.implementation.PerformerControllerImpl;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MenuDeletePerformer implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private PerformerControllerImpl performerController;
    private Logger log;

    public MenuDeletePerformer() {
        performerController = new PerformerControllerImpl();
        log = LogManager.getLogger(MenuDeletePerformer.class);
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Delete data by id.");
        menuDescription.put("2", " 2 - Delete data by name.");
        menuDescription.put("3", " 3 - Delete data by surname.");
        menuDescription.put("4", " 4 - Delete data by data born.");
        menuDescription.put("5", " 5 - Delete data by music disc id.");
        menuDescription.put(BACK_POINT, " " + BACK_POINT + " - Back to performer menu.");
    }

    private void initMenuMethods() {
        menuMethods.put("1", performerController::delete);
        menuMethods.put("2", performerController::deleteByName);
        menuMethods.put("3", performerController::deleteBySurname);
        menuMethods.put("4", performerController::deleteByDateBorn);
        menuMethods.put("5", performerController::deleteByMusicDiscID);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(MENU_DELETE_BY_PERFORMER);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(BACK_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}
