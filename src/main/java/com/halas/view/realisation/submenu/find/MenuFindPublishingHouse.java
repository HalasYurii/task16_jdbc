package com.halas.view.realisation.submenu.find;

import com.halas.controller.implementation.PublishingHouseControllerImpl;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MenuFindPublishingHouse implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private PublishingHouseControllerImpl publishingHouseController;
    private Logger log;

    public MenuFindPublishingHouse() {
        publishingHouseController = new PublishingHouseControllerImpl();
        log = LogManager.getLogger(MenuFindPublishingHouse.class);
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Show data by id.");
        menuDescription.put("2", " 2 - Show data by name.");
        menuDescription.put("3", " 3 - Show data by date born.");
        menuDescription.put(BACK_POINT, " " + BACK_POINT + " - Back to publishing house menu.");
    }

    private void initMenuMethods() {
        menuMethods.put("1", publishingHouseController::showById);
        menuMethods.put("2", publishingHouseController::showByName);
        menuMethods.put("3", publishingHouseController::showByDateBorn);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(MENU_FIND_BY_PUBLISHING_HOUSE);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(BACK_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}
