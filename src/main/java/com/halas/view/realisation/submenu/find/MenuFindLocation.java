package com.halas.view.realisation.submenu.find;

import com.halas.controller.implementation.LocationControllerImpl;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MenuFindLocation implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private LocationControllerImpl locationController;
    private Logger log;

    public MenuFindLocation() {
        locationController = new LocationControllerImpl();
        log = LogManager.getLogger(MenuFindLocation.class);
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Show data by id.");
        menuDescription.put("2", " 2 - Show data by country.");
        menuDescription.put("3", " 3 - Show data by address.");
        menuDescription.put("4", " 4 - Show data by publishing house.");
        menuDescription.put(BACK_POINT, " " + BACK_POINT + " - Back to location menu.");
    }

    private void initMenuMethods() {
        menuMethods.put("1", locationController::showById);
        menuMethods.put("2", locationController::showByCountry);
        menuMethods.put("3", locationController::showByAddress);
        menuMethods.put("4", locationController::showByPublishingHouseID);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(MENU_FIND_BY_LOCATION);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(BACK_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}
