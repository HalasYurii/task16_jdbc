package com.halas.view.realisation;

import com.halas.controller.ShowAllDataTables;
import com.halas.structure.StructureDB;
import com.halas.view.Menu;
import com.halas.view.functional.Printable;
import com.halas.view.realisation.submenu.concrete.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.constants.Constants.INPUT;
import static com.halas.constants.GeneralConstantMenu.*;

public class MainMenu implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Printable> menuMethods;
    private Logger log;

    public MainMenu() {
        log = LogManager.getLogger(MainMenu.class);
        initMenu();
    }

    private void initDescriptionsMenu() {
        menuDescription.put("1", " 1 - Show data from all tables.");
        menuDescription.put("2", " 2 - Show name of all tables.");
        menuDescription.put("3", " 3 - Show structure of DB.");
        menuDescription.put("4", " 4 - Go to table: \'location\'.");
        menuDescription.put("5", " 5 - Go to table: \'music_disc\'.");
        menuDescription.put("6", " 6 - Go to table: \'performer\'.");
        menuDescription.put("7", " 7 - Go to table: \'publishing_house\'.");
        menuDescription.put("8", " 8 - Go to table: \'song\'.");
        menuDescription.put(EXIT_POINT, String.format(" %s - Exit.", EXIT_POINT));
    }

    private void initMethodsMenu() {
        menuMethods.put("1", new ShowAllDataTables()::show);
        menuMethods.put("2", new StructureDB()::getAllTableName);
        menuMethods.put("3", new StructureDB()::getStructureDB);
        menuMethods.put("4", new MenuLocation()::run);
        menuMethods.put("5", new MenuMusicDisc()::run);
        menuMethods.put("6", new MenuPerformer()::run);
        menuMethods.put("7", new MenuPublishingHouse()::run);
        menuMethods.put("8", new MenuSong()::run);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initDescriptionsMenu();
        initMethodsMenu();
    }

    private void outputMenu() {
        log.debug(MAIN_MENU);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void run() {
        String key;
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                log.info("\n\n");
                if (key.equals(EXIT_POINT)) {
                    break;
                }
                menuMethods.get(key).print();
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            } catch (NullPointerException e) {
                log.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}