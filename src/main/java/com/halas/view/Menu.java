package com.halas.view;

/**
 * to Show menu and run submenu.
 */
public interface Menu {
    void run();
}