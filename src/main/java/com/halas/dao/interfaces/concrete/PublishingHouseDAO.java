package com.halas.dao.interfaces.concrete;

import com.halas.dao.interfaces.GeneralDAO;
import com.halas.dao.interfaces.delete.PublishingHouseDeleteDAO;
import com.halas.dao.interfaces.find.PublishingHouseFindDAO;
import com.halas.model.PublishingHouse;

public interface PublishingHouseDAO extends
        GeneralDAO<PublishingHouse, Integer>,
        PublishingHouseFindDAO,
        PublishingHouseDeleteDAO {
}
