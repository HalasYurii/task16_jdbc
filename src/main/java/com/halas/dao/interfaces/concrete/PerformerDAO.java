package com.halas.dao.interfaces.concrete;

import com.halas.dao.interfaces.GeneralDAO;
import com.halas.dao.interfaces.delete.PerformerDeleteDAO;
import com.halas.dao.interfaces.find.PerformerFindDAO;
import com.halas.model.Performer;

public interface PerformerDAO extends
        GeneralDAO<Performer, Integer>,
        PerformerFindDAO,
        PerformerDeleteDAO {
}
