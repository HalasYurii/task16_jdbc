package com.halas.dao.interfaces.concrete;

import com.halas.dao.interfaces.GeneralDAO;
import com.halas.dao.interfaces.delete.LocationDeleteDAO;
import com.halas.dao.interfaces.find.LocationFindDAO;
import com.halas.model.Location;

public interface LocationDAO extends
        GeneralDAO<Location, Integer>,
        LocationFindDAO,
        LocationDeleteDAO {
}


