package com.halas.dao.interfaces.concrete;

import com.halas.dao.interfaces.GeneralDAO;
import com.halas.dao.interfaces.delete.MusicDiscDeleteDAO;
import com.halas.dao.interfaces.find.MusicDiscFindDAO;
import com.halas.model.MusicDisc;

public interface MusicDiscDAO extends
        GeneralDAO<MusicDisc, Integer>,
        MusicDiscFindDAO,
        MusicDiscDeleteDAO {

}
