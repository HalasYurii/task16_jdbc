package com.halas.dao.interfaces.concrete;

import com.halas.dao.interfaces.GeneralDAO;
import com.halas.dao.interfaces.delete.SongDeleteDAO;
import com.halas.dao.interfaces.find.SongFindDAO;
import com.halas.model.Song;

public interface SongDAO extends
        GeneralDAO<Song, Integer>,
        SongFindDAO,
        SongDeleteDAO {
}
