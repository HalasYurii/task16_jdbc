package com.halas.dao.interfaces.find;

import com.halas.model.Song;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface SongFindDAO {
    List<Song> findByName(String name) throws SQLException;

    List<Song> findByDateOut(Date date) throws SQLException;

    List<Song> findByPerformerID(Integer id) throws SQLException;
}
