package com.halas.dao.interfaces.find;

import com.halas.model.PublishingHouse;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface PublishingHouseFindDAO {
    List<PublishingHouse> findByName(String name) throws SQLException;

    List<PublishingHouse> findByDateBorn(Date date) throws SQLException;
}
