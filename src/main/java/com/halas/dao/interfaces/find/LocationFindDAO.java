package com.halas.dao.interfaces.find;

import com.halas.model.Location;

import java.sql.SQLException;
import java.util.List;

public interface LocationFindDAO {
    List<Location> findByCountry(String country) throws SQLException;

    List<Location> findByAddress(String address) throws SQLException;

    List<Location> findByPublishingHouseID(Integer id) throws SQLException;
}
