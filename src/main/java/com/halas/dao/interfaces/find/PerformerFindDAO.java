package com.halas.dao.interfaces.find;

import com.halas.model.Performer;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface PerformerFindDAO {
    List<Performer> findByName(String name) throws SQLException;

    List<Performer> findBySurname(String surname) throws SQLException;

    List<Performer> findByDateBorn(Date date) throws SQLException;

    List<Performer> findByMusicDiscID(Integer id) throws SQLException;
}
