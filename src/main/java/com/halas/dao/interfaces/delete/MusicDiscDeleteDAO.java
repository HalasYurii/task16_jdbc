package com.halas.dao.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface MusicDiscDeleteDAO {
    int deleteByName(String name) throws SQLException;

    int deleteByType(String type) throws SQLException;

    int deleteByDateOut(Date date) throws SQLException;

    int deleteByPublishingHouseID(Integer id) throws SQLException;
}
