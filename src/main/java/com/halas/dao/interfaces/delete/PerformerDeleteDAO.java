package com.halas.dao.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface PerformerDeleteDAO {
    int deleteByName(String name) throws SQLException;

    int deleteBySurname(String surname) throws SQLException;

    int deleteByDateBorn(Date date) throws SQLException;

    int deleteByMusicDiscID(Integer id) throws SQLException;
}
