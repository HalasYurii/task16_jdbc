package com.halas.dao.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface SongDeleteDAO {
    int deleteByName(String name) throws SQLException;

    int deleteByDateOut(Date date) throws SQLException;

    int deleteByPerformerID(Integer id) throws SQLException;
}
