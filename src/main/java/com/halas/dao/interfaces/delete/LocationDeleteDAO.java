package com.halas.dao.interfaces.delete;

import java.sql.SQLException;

public interface LocationDeleteDAO {
    int deleteByCountry(String country) throws SQLException;

    int deleteByAddress(String address) throws SQLException;

    int deleteByPublishingHouseID(Integer id) throws SQLException;
}
