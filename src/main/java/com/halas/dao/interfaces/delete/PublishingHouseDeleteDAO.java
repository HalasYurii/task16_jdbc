package com.halas.dao.interfaces.delete;

import java.sql.Date;
import java.sql.SQLException;

public interface PublishingHouseDeleteDAO {
    int deleteByName(String name) throws SQLException;

    int deleteByDateBorn(Date date) throws SQLException;
}
