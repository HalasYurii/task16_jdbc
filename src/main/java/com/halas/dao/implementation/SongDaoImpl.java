package com.halas.dao.implementation;

import com.halas.dao.interfaces.concrete.SongDAO;
import com.halas.model.Song;
import com.halas.persistant.ConnectionManager;
import com.halas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.halas.constants.queries.SongQuery.*;

public class SongDaoImpl implements SongDAO {

    private Connection connection;

    public SongDaoImpl() {
        connection = ConnectionManager.getConnection();
    }

    @Override
    public List<Song> findAll() throws SQLException {
        List<Song> songs = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    songs.add((Song) new Transformer(Song.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return songs;
    }

    @Override
    public Song findById(Integer id) throws SQLException {
        Song entity = null;
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (Song) new Transformer(Song.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public List<Song> findByName(String name) throws SQLException {
        List<Song> songs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    Song song = (Song) new Transformer(Song.class).fromResultSetToEntity(resultSet);
                    songs.add(song);
                }
            }
        }
        return songs;
    }

    @Override
    public List<Song> findByDateOut(Date date) throws SQLException {
        List<Song> songs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DATE_OUT)) {
            ps.setDate(1, date);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    Song song = (Song) new Transformer(Song.class).fromResultSetToEntity(resultSet);
                    songs.add(song);
                }
            }
        }
        return songs;
    }

    @Override
    public List<Song> findByPerformerID(Integer id) throws SQLException {
        List<Song> songs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_PERFORMER_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    Song song = (Song) new Transformer(Song.class).fromResultSetToEntity(resultSet);
                    songs.add(song);
                }
            }
        }
        return songs;
    }

    @Override
    public int create(Song entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setDate(3, entity.getDateOut());
            ps.setInt(4, entity.getPerformerId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Song entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(4, entity.getId());
            ps.setInt(3, entity.getPerformerId());
            ps.setString(1, entity.getName());
            ps.setDate(2, entity.getDateOut());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_NAME)) {
            ps.setString(1, name);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByDateOut(Date date) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_DATE_OUT)) {
            ps.setDate(1, date);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByPerformerID(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_PERFORMER_ID)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}