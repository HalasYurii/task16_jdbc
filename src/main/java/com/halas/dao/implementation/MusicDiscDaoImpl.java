package com.halas.dao.implementation;

import com.halas.dao.interfaces.concrete.MusicDiscDAO;
import com.halas.model.MusicDisc;
import com.halas.persistant.ConnectionManager;
import com.halas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.halas.constants.queries.MusicDiscQuery.*;

public class MusicDiscDaoImpl implements MusicDiscDAO {

    private Connection connection;

    public MusicDiscDaoImpl() {
        connection = ConnectionManager.getConnection();
    }

    @Override
    public List<MusicDisc> findAll() throws SQLException {
        List<MusicDisc> discs = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    discs.add((MusicDisc) new Transformer(MusicDisc.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return discs;
    }

    @Override
    public MusicDisc findById(Integer id) throws SQLException {
        MusicDisc entity = null;
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (MusicDisc) new Transformer(MusicDisc.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public List<MusicDisc> findByType(String type) throws SQLException {
        List<MusicDisc> musicDiscs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_TYPE)) {
            ps.setString(1, type);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    MusicDisc entity = (MusicDisc) new Transformer(MusicDisc.class).fromResultSetToEntity(resultSet);
                    musicDiscs.add(entity);
                }
            }
        }
        return musicDiscs;
    }

    @Override
    public List<MusicDisc> findByName(String name) throws SQLException {
        List<MusicDisc> musicDiscs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    MusicDisc entity = (MusicDisc) new Transformer(MusicDisc.class).fromResultSetToEntity(resultSet);
                    musicDiscs.add(entity);
                }
            }
        }
        return musicDiscs;
    }

    @Override
    public List<MusicDisc> findByDateOut(Date date) throws SQLException {
        List<MusicDisc> musicDiscs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DATE_OUT)) {
            ps.setDate(1, date);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    MusicDisc entity = (MusicDisc) new Transformer(MusicDisc.class).fromResultSetToEntity(resultSet);
                    musicDiscs.add(entity);
                }
            }
        }
        return musicDiscs;
    }

    @Override
    public List<MusicDisc> findByPublishingHouseID(Integer id) throws SQLException {
        List<MusicDisc> musicDiscs = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_PUBLISHING_HOUSE_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    MusicDisc entity = (MusicDisc) new Transformer(MusicDisc.class).fromResultSetToEntity(resultSet);
                    musicDiscs.add(entity);
                }
            }
        }
        return musicDiscs;
    }

    @Override
    public int create(MusicDisc entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setString(3, entity.getType());
            ps.setDate(4, entity.getDateOut());
            ps.setInt(5, entity.getPublishingHouseId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(MusicDisc entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getType());
            ps.setDate(3, entity.getDateOut());
            ps.setInt(4, entity.getPublishingHouseId());
            ps.setInt(5, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_NAME)) {
            ps.setString(1, name);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByType(String type) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_TYPE)) {
            ps.setString(1, type);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByDateOut(Date date) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_DATE_OUT)) {
            ps.setDate(1, date);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByPublishingHouseID(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_PUBLISHING_HOUSE_ID)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}