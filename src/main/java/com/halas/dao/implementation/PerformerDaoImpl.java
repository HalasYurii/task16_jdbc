package com.halas.dao.implementation;

import com.halas.dao.interfaces.concrete.PerformerDAO;
import com.halas.model.Performer;
import com.halas.persistant.ConnectionManager;
import com.halas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.halas.constants.queries.PerformerQuery.*;

public class PerformerDaoImpl implements PerformerDAO {

    private Connection connection;

    public PerformerDaoImpl() {
        connection = ConnectionManager.getConnection();
    }

    @Override
    public List<Performer> findAll() throws SQLException {
        List<Performer> performers = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    performers.add((Performer) new Transformer(Performer.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return performers;
    }

    @Override
    public Performer findById(Integer id) throws SQLException {
        Performer entity = null;
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (Performer) new Transformer(Performer.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public List<Performer> findByName(String name) throws SQLException {
        List<Performer> performers = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    performers.add((Performer) new Transformer(Performer.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return performers;
    }

    @Override
    public List<Performer> findBySurname(String surname) throws SQLException {
        List<Performer> performers = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_SURNAME)) {
            ps.setString(1, surname);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    performers.add((Performer) new Transformer(Performer.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return performers;
    }

    @Override
    public List<Performer> findByDateBorn(Date date) throws SQLException {
        List<Performer> performers = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DATE_BORN)) {
            ps.setDate(1, date);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    performers.add((Performer) new Transformer(Performer.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return performers;
    }

    @Override
    public List<Performer> findByMusicDiscID(Integer id) throws SQLException {
        List<Performer> performers = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_MUSIC_DISC_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    performers.add((Performer) new Transformer(Performer.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return performers;
    }

    @Override
    public int create(Performer entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setString(3, entity.getSurname());
            ps.setDate(4, entity.getDateBorn());
            ps.setInt(5, entity.getMusicDiscId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Performer entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getSurname());
            ps.setDate(3, entity.getDateBorn());
            ps.setInt(4, entity.getMusicDiscId());
            ps.setInt(5, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_NAME)) {
            ps.setString(1, name);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteBySurname(String surname) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_SURNAME)) {
            ps.setString(1, surname);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByDateBorn(Date date) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_DATE_BORN)) {
            ps.setDate(1, date);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByMusicDiscID(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_MUSIC_DISC_ID)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}