package com.halas.dao.implementation;

import com.halas.dao.interfaces.concrete.PublishingHouseDAO;
import com.halas.model.PublishingHouse;
import com.halas.persistant.ConnectionManager;
import com.halas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.halas.constants.queries.PublishingHouseQuery.*;

public class PublishingHouseDaoImpl implements PublishingHouseDAO {

    private Connection connection;

    public PublishingHouseDaoImpl() {
        connection = ConnectionManager.getConnection();
    }

    @Override
    public List<PublishingHouse> findAll() throws SQLException {
        List<PublishingHouse> employees = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    employees.add((PublishingHouse) new Transformer(PublishingHouse.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }

    @Override
    public PublishingHouse findById(Integer id) throws SQLException {
        PublishingHouse entity = null;
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (PublishingHouse) new Transformer(PublishingHouse.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public List<PublishingHouse> findByName(String name) throws SQLException {
        List<PublishingHouse> publishingHouses = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    PublishingHouse publishingHouse =
                            (PublishingHouse) new Transformer(PublishingHouse.class)
                                    .fromResultSetToEntity(resultSet);
                    publishingHouses.add(publishingHouse);
                }
            }
        }
        return publishingHouses;
    }

    @Override
    public List<PublishingHouse> findByDateBorn(Date date) throws SQLException {
        List<PublishingHouse> publishingHouses = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DATE_BORN)) {
            ps.setDate(1, date);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    PublishingHouse publishingHouse =
                            (PublishingHouse) new Transformer(PublishingHouse.class)
                                    .fromResultSetToEntity(resultSet);
                    publishingHouses.add(publishingHouse);
                }
            }
        }
        return publishingHouses;
    }

    @Override
    public int create(PublishingHouse entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setDate(3, entity.getDateBorn());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(PublishingHouse entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(3, entity.getId());
            ps.setDate(2, entity.getDateBorn());
            ps.setString(1, entity.getName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByName(String name) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_NAME)) {
            ps.setString(1, name);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByDateBorn(Date date) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_DATE_BORN)) {
            ps.setDate(1, date);
            return ps.executeUpdate();
        }
    }
}