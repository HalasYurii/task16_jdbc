package com.halas.dao.implementation.meta;


import com.halas.model.meta.ColumnMeta;
import com.halas.model.meta.ForeignKeyMeta;
import com.halas.model.meta.TableMeta;
import com.halas.persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetaDaoImpl {
    public List<String> findAllTableName() throws SQLException {
        List<String> tableNames = new ArrayList<>();
        String[] types = {"TABLE"};
        Connection connection = ConnectionManager.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet result = databaseMetaData.getTables(connection.getCatalog(), null, "%", types);

        while (result.next()) {
            String tableName = result.getString("TABLE_PUBLISHING_HOUSE");
            tableNames.add(tableName);
        }
        return tableNames;
    }

    public List<TableMeta> getTablesStructure() throws SQLException {
        List<TableMeta> tableMetaDataList = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();

        String[] types = {"TABLE"};
        String dbName = connection.getCatalog();
        ResultSet result = databaseMetaData.getTables(dbName, null, "%", types);

        while (result.next()) {
            String tableName = result.getString("TABLE_PUBLISHING_HOUSE");
            TableMeta tableMetaData = new TableMeta();
            tableMetaData.setDBName(dbName);
            tableMetaData.setTableName(tableName);

            List<String> pkList = new ArrayList<>();
            ResultSet pkS = databaseMetaData.getPrimaryKeys(connection.getCatalog(), null, tableName);
            while (pkS.next()) {
                pkList.add(pkS.getString("COLUMN_NAME"));
            }

            List<ColumnMeta> columnsMetaData = getColumnData(pkList, dbName, databaseMetaData, tableName);
            tableMetaData.setColumnMetaData(columnsMetaData);

            List<ForeignKeyMeta> fkFromTable = getFk(dbName, databaseMetaData, tableName);
            tableMetaData.setForeignKeyList(fkFromTable);
            tableMetaDataList.add(tableMetaData);
        }
        return tableMetaDataList;
    }

    private List<ForeignKeyMeta> getFk(String dbName, DatabaseMetaData databaseMetaData, String tableName)
            throws SQLException {
        List<ForeignKeyMeta> fkMetaDataList = new ArrayList<>();
        ResultSet fKsRS = databaseMetaData.getImportedKeys(dbName, null, tableName);
        while (fKsRS.next()) {
            ForeignKeyMeta fk = new ForeignKeyMeta();
            fk.setFkColumnName(fKsRS.getString("FKCOLUMN_NAME"));
            fk.setPkTableName(fKsRS.getString("PKTABLE_NAME"));
            fk.setPkColumnName(fKsRS.getString("PKCOLUMN_NAME"));
            fkMetaDataList.add(fk);
        }
        return fkMetaDataList;
    }

    private List<ColumnMeta> getColumnData(List<String> pkList,
                                           String dbName,
                                           DatabaseMetaData databaseMetaData,
                                           String tableName) throws SQLException {
        List<ColumnMeta> columnsMetaData = new ArrayList<>();
        ResultSet columnsRS = databaseMetaData.getColumns(dbName, null, tableName, "%");
        while (columnsRS.next()) {
            ColumnMeta columnMetaData = new ColumnMeta();
            setToRS(columnMetaData, columnsRS);
            columnMetaData.setPrimaryKey(false);
            for (String pkName : pkList) {
                if (columnMetaData.getColumnName().equals(pkName)) {
                    columnMetaData.setPrimaryKey(true);
                    break;
                }
            }
            columnsMetaData.add(columnMetaData);
        }
        return columnsMetaData;
    }

    private void setToRS(ColumnMeta columnMetaData, ResultSet columnsRS) throws SQLException {
        columnMetaData.setColumnName(columnsRS.getString("COLUMN_NAME"));
        columnMetaData.setDataType(columnsRS.getString("TYPE_NAME"));
        columnMetaData.setColumnSize(columnsRS.getString("COLUMN_SIZE"));
        columnMetaData.setDecimalDigits(columnsRS.getString("DECIMAL_DIGITS"));
        boolean cond = columnsRS.getString("IS_NULLABLE").equals("YES");
        columnMetaData.setNullable(cond);
        cond = columnsRS.getString("IS_AUTOINCREMENT").equals("IS_AUTOINCREMENT");
        columnMetaData.setAutoIncrement(cond);
    }
}
