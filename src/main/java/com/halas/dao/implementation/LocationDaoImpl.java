package com.halas.dao.implementation;

import com.halas.dao.interfaces.concrete.LocationDAO;
import com.halas.model.Location;
import com.halas.persistant.ConnectionManager;
import com.halas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.halas.constants.queries.LocationQuery.*;

public class LocationDaoImpl implements LocationDAO {
    private Connection connection;

    public LocationDaoImpl() {
        connection = ConnectionManager.getConnection();
    }

    @Override
    public List<Location> findAll() throws SQLException {
        List<Location> locations = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    locations.add((Location) new Transformer(Location.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return locations;
    }

    @Override
    public Location findById(Integer id) throws SQLException {
        Location entity = null;
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (Location) new Transformer(Location.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public List<Location> findByCountry(String country) throws SQLException {
        List<Location> locations = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_COUNTRY)) {
            ps.setString(1, country);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    Location location = (Location) new Transformer(Location.class).fromResultSetToEntity(resultSet);
                    locations.add(location);
                }
            }
        }
        return locations;
    }

    @Override
    public List<Location> findByAddress(String address) throws SQLException {
        List<Location> locations = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ADDRESS)) {
            ps.setString(1, address);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    Location location = (Location) new Transformer(Location.class).fromResultSetToEntity(resultSet);
                    locations.add(location);
                }
            }
        }
        return locations;
    }

    @Override
    public List<Location> findByPublishingHouseID(Integer id) throws SQLException {
        List<Location> locations = new LinkedList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_PUBLISHING_HOUSE_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    Location location = (Location) new Transformer(Location.class).fromResultSetToEntity(resultSet);
                    locations.add(location);
                }
            }
        }
        return locations;
    }

    @Override
    public int create(Location entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getCountry());
            ps.setString(3, entity.getAddress());
            ps.setInt(4, entity.getPublishingHouseId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Location entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getCountry());
            ps.setString(2, entity.getAddress());
            ps.setInt(3, entity.getPublishingHouseId());
            ps.setInt(4, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByCountry(String country) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_COUNTRY)) {
            ps.setString(1, country);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByAddress(String address) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_ADDRESS)) {
            ps.setString(1, address);
            return ps.executeUpdate();
        }
    }

    @Override
    public int deleteByPublishingHouseID(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_PUBLISHING_HOUSE_ID)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}