package com.halas.structure;


import com.halas.model.meta.TableMeta;
import com.halas.persistant.ConnectionManager;
import com.halas.service.implementation.meta.MetaServiceImpl;
import com.halas.service.interfaces.concrete.meta.MetaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static com.halas.constants.Constants.NAME_SCHEMA_BD;

public class StructureDB {
    private Logger log;

    public StructureDB() {
        log = LogManager.getLogger(StructureDB.class);
    }

    public void getStructureDB() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaService metaDataService = new MetaServiceImpl();
        List<TableMeta> tables = metaDataService.getTablesStructure();
        log.info(NAME_SCHEMA_BD + connection.getCatalog());
        tables.forEach(log::info);
    }

    public void getAllTableName() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaService metaDataService = new MetaServiceImpl();
        List<String> tables = metaDataService.findAllTableName();
        log.info(NAME_SCHEMA_BD + connection.getCatalog());
        tables.forEach(log::info);
    }
}