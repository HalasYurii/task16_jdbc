package com.halas.model;

import com.halas.model.annotation.Column;
import com.halas.model.annotation.ForeignKey;
import com.halas.model.annotation.PrimaryKey;
import com.halas.model.annotation.Table;

import java.sql.Date;

@Table(name = "music_disc")
public class MusicDisc {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name", length = 25)
    private String name;
    @Column(name = "type", length = 15)
    private String type;
    @Column(name = "date_out")
    private Date dateOut;
    @ForeignKey
    @Column(name = "publishing_house_id")
    private Integer publishingHouseId;

    public MusicDisc() {
    }

    public MusicDisc(Integer id, String name, String type, Date dateOut, Integer publishingHouseId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.dateOut = dateOut;
        this.publishingHouseId = publishingHouseId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateOut() {
        return dateOut;
    }

    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }

    public Integer getPublishingHouseId() {
        return publishingHouseId;
    }

    public void setPublishingHouseId(Integer publishingHouseId) {
        this.publishingHouseId = publishingHouseId;
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-15s %-15s %d", id, name, type, dateOut, publishingHouseId);
    }
}
