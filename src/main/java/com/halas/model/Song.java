package com.halas.model;

import com.halas.model.annotation.Column;
import com.halas.model.annotation.ForeignKey;
import com.halas.model.annotation.PrimaryKey;
import com.halas.model.annotation.Table;

import java.sql.Date;

@Table(name = "works_on")
public class Song {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name", length = 25)
    private String name;
    @Column(name = "date_out")
    private Date dateOut;
    @ForeignKey
    @Column(name = "performer_id")
    private Integer performerId;

    public Song() {
    }

    public Song(Integer id, String name, Date dateOut, Integer performerId) {
        this.id = id;
        this.name = name;
        this.dateOut = dateOut;
        this.performerId = performerId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOut() {
        return dateOut;
    }

    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }

    public Integer getPerformerId() {
        return performerId;
    }

    public void setPerformerId(Integer performerId) {
        this.performerId = performerId;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-30s %-15s %d", id, name, dateOut, performerId);
    }
}
