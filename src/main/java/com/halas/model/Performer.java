package com.halas.model;

import com.halas.model.annotation.Column;
import com.halas.model.annotation.ForeignKey;
import com.halas.model.annotation.PrimaryKey;
import com.halas.model.annotation.Table;

import java.sql.Date;

@Table(name = "performer")
public class Performer {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "surname", length = 45)
    private String surname;
    @Column(name = "date_born")
    private Date dateBorn;
    @ForeignKey
    @Column(name = "music_disc_id")
    private Integer musicDiscId;

    public Performer() {
    }

    public Performer(Integer id, String name, String surname, Date date, Integer musicDiscId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.dateBorn = date;
        this.musicDiscId = musicDiscId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateBorn() {
        return dateBorn;
    }

    public void setDateBorn(Date dateBorn) {
        this.dateBorn = dateBorn;
    }

    public Integer getMusicDiscId() {
        return musicDiscId;
    }

    public void setMusicDiscId(Integer musicDiscId) {
        this.musicDiscId = musicDiscId;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-15s %-15s %d", id, name, surname, dateBorn, musicDiscId);
    }
}
