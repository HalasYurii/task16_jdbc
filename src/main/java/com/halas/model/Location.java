package com.halas.model;

import com.halas.model.annotation.Column;
import com.halas.model.annotation.ForeignKey;
import com.halas.model.annotation.PrimaryKey;
import com.halas.model.annotation.Table;

@Table(name = "address")
public class Location {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "country", length = 25)
    private String country;
    @Column(name = "address", length = 45)
    private String address;
    @ForeignKey
    @Column(name = "publishing_house_id")
    private Integer publishingHouseId;

    public Location() {
    }

    public Location(Integer deptNo, String deptName, String location) {
        this.id = deptNo;
        this.country = deptName;
        this.address = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPublishingHouseId() {
        return publishingHouseId;
    }

    public void setPublishingHouseId(Integer publishingHouseId) {
        this.publishingHouseId = publishingHouseId;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-25s %d", id, country, address, publishingHouseId);
    }
}