package com.halas.model.meta;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TableMeta {
    private String DBName;
    private String tableName;
    private List<ColumnMeta> columnMetaData = new ArrayList<>();
    private List<ForeignKeyMeta> foreignKeyList = new ArrayList<>();

    public void setDBName(String DBName) {
        this.DBName = DBName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setColumnMetaData(List<ColumnMeta> columnMetaData) {
        this.columnMetaData = columnMetaData;
    }

    public void setForeignKeyList(List<ForeignKeyMeta> foreignKeyList) {
        this.foreignKeyList = foreignKeyList;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TABLE: ")
                .append(tableName)
                .append("\n");
        for (ColumnMeta column : columnMetaData) {
            stringBuilder.append(column)
                    .append("\n");
        }
        Set<String> uniqueFkList = foreignKeyList.stream().map(ForeignKeyMeta::toString).collect(Collectors.toSet());
        for (String fk : uniqueFkList) {
            stringBuilder.append(fk)
                    .append("\n");
        }
        return stringBuilder.toString();
    }

}
