package com.halas.model.meta;

public class ForeignKeyMeta {
    private String pkColumnName;
    private String pkTableName;
    private String fkColumnName;

    public void setPkColumnName(String pkColumnName) {
        this.pkColumnName = pkColumnName;
    }

    public void setPkTableName(String pkTableName) {
        this.pkTableName = pkTableName;
    }

    public void setFkColumnName(String fkColumnName) {
        this.fkColumnName = fkColumnName;
    }

    @Override
    public String toString() {
        return "FK_COLUMN: " + fkColumnName + ",  PK_TABLE: " + pkTableName + ",  PK_COLUMN_NAME: " + pkColumnName;
    }
}
