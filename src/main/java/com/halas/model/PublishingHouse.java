package com.halas.model;

import com.halas.model.annotation.Column;
import com.halas.model.annotation.PrimaryKey;
import com.halas.model.annotation.Table;

import java.sql.Date;

@Table(name = "publishing_house")
public class PublishingHouse {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "date_born")
    private Date dateBorn;

    public PublishingHouse() {
    }

    public PublishingHouse(Integer id, String name, Date dateBorn) {
        this.id = id;
        this.name = name;
        this.dateBorn = dateBorn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateBorn() {
        return dateBorn;
    }

    public void setDateBorn(Date dateBorn) {
        this.dateBorn = dateBorn;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %s", id, name, dateBorn);
    }
}
