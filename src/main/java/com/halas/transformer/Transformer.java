package com.halas.transformer;


import com.halas.model.annotation.Column;
import com.halas.model.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.*;

public class Transformer<T> {
    private final Class<T> clazz;


    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    private void setField(Object shop, ResultSet rs, String name, Field field)
            throws SQLException, IllegalAccessException {
        Class fieldType = field.getType();
        if (fieldType == String.class) {
            field.set(shop, rs.getString(name));
        } else if (fieldType == Integer.class) {
            field.set(shop, rs.getInt(name));
        } else if (fieldType == Date.class) {
            field.set(shop, rs.getDate(name));
        }
    }

    public Object fromResultSetToEntity(ResultSet rs) throws SQLException {
        Object shop = null;
        try {
            shop = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        setField(shop,rs,name,field);
                    }
                }
            }
        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException ignored) {
        }
        return shop;
    }
}