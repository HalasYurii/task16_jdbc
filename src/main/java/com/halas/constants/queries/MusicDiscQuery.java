package com.halas.constants.queries;

public class MusicDiscQuery {
    public static final String FIND_ALL = "SELECT id,name,type,date_out,publishing_house_id FROM music_disc";
    public static final String DELETE = "DELETE FROM music_disc WHERE id=?";
    public static final String CREATE = "INSERT music_disc (id, name, type, date_out, publishing_house_id) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE = "UPDATE music_disc SET name=?, type=?, date_out=?, publishing_house_id=? WHERE id=?";

    public static final String FIND_BY_ID = "SELECT id,name,type,date_out,publishing_house_id FROM music_disc WHERE id=?";
    public static final String FIND_BY_NAME = "SELECT id,name,type,date_out,publishing_house_id FROM music_disc WHERE name=?";
    public static final String FIND_BY_TYPE = "SELECT id,name,type,date_out,publishing_house_id FROM music_disc WHERE type=?";
    public static final String FIND_BY_DATE_OUT = "SELECT id,name,type,date_out,publishing_house_id FROM music_disc WHERE date_out=?";
    public static final String FIND_BY_PUBLISHING_HOUSE_ID = "SELECT id,name,type,date_out,publishing_house_id FROM music_disc WHERE publishing_house_id=?";

    public static final String DELETE_BY_NAME = "DELETE FROM music_disc WHERE name=?";
    public static final String DELETE_BY_TYPE = "DELETE FROM music_disc WHERE type=?";
    public static final String DELETE_BY_DATE_OUT = "DELETE FROM music_disc WHERE date_out=?";
    public static final String DELETE_BY_PUBLISHING_HOUSE_ID = "DELETE FROM music_disc WHERE publishing_house_id=?";

    private MusicDiscQuery() {
    }
}
