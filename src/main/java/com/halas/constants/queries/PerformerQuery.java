package com.halas.constants.queries;

public class PerformerQuery {
    public static final String FIND_ALL = "SELECT id,name,surname,date_born,music_disc_id FROM performer";
    public static final String DELETE = "DELETE FROM performer WHERE id=?";
    public static final String CREATE = "INSERT performer (id, name, surname, date_born, music_disc_id) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE = "UPDATE performer SET name=?, surname=?, date_born=?, music_disc_id=? WHERE id=?";

    public static final String FIND_BY_ID = "SELECT id,name,surname,date_born,music_disc_id FROM performer WHERE id=?";
    public static final String FIND_BY_NAME = "SELECT id,name,surname,date_born,music_disc_id FROM performer WHERE name=?";
    public static final String FIND_BY_SURNAME = "SELECT id,name,surname,date_born,music_disc_id FROM performer WHERE surname=?";
    public static final String FIND_BY_DATE_BORN = "SELECT id,name,surname,date_born,music_disc_id FROM performer WHERE date_born=?";
    public static final String FIND_BY_MUSIC_DISC_ID = "SELECT id,name,surname,date_born,music_disc_id FROM performer WHERE music_disc_id=?";

    public static final String DELETE_BY_NAME = "DELETE FROM performer WHERE name=?";
    public static final String DELETE_BY_SURNAME = "DELETE FROM performer WHERE surname=?";
    public static final String DELETE_BY_DATE_BORN = "DELETE FROM performer WHERE date_born=?";
    public static final String DELETE_BY_MUSIC_DISC_ID = "DELETE FROM performer WHERE music_disc_id=?";

    private PerformerQuery() {
    }
}
