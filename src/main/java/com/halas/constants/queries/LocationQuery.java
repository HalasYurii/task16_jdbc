package com.halas.constants.queries;

public class LocationQuery {
    public static final String FIND_ALL = "SELECT id,country,address,publishing_house_id FROM location";
    public static final String DELETE = "DELETE FROM location WHERE id=?";
    public static final String CREATE = "INSERT location (id, country, address, publishing_house_id) VALUES (?, ?, ?, ?)";
    public static final String UPDATE = "UPDATE location SET country=?, address=?, publishing_house_id=? WHERE id=?";

    public static final String FIND_BY_ID = "SELECT id,country,address,publishing_house_id FROM location WHERE id=?";
    public static final String FIND_BY_COUNTRY = "SELECT id,country,address,publishing_house_id FROM location WHERE country=?";
    public static final String FIND_BY_ADDRESS = "SELECT id,country,address,publishing_house_id FROM location WHERE address=?";
    public static final String FIND_BY_PUBLISHING_HOUSE_ID = "SELECT id,country,address,publishing_house_id FROM location WHERE publishing_house_id=?";

    public static final String DELETE_BY_COUNTRY = "DELETE FROM location WHERE country=?";
    public static final String DELETE_BY_ADDRESS = "DELETE FROM location WHERE address=?";
    public static final String DELETE_BY_PUBLISHING_HOUSE_ID = "DELETE FROM location WHERE publishing_house_id=?";

    private LocationQuery() {
    }
}
