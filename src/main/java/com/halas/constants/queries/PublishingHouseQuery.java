package com.halas.constants.queries;

public class PublishingHouseQuery {
    public static final String FIND_ALL = "SELECT id,name,date_born FROM publishing_house";
    public static final String DELETE = "DELETE FROM publishing_house WHERE id=?";
    public static final String CREATE = "INSERT publishing_house (id, name, date_born) VALUES (?, ?, ?)";
    public static final String UPDATE = "UPDATE publishing_house SET name=?, date_born=? WHERE id=?";

    public static final String FIND_BY_ID = "SELECT id,name,date_born FROM publishing_house WHERE id=?";
    public static final String FIND_BY_NAME = "SELECT id,name,date_born FROM publishing_house WHERE name=?";
    public static final String FIND_BY_DATE_BORN = "SELECT id,name,date_born FROM publishing_house WHERE date_born=?";

    public static final String DELETE_BY_NAME = "DELETE FROM publishing_house WHERE name=?";
    public static final String DELETE_BY_DATE_BORN = "DELETE FROM publishing_house WHERE date_born=?";

    private PublishingHouseQuery() {
    }
}
