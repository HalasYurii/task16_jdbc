package com.halas.constants.queries;

public class SongQuery {
    public static final String FIND_ALL = "SELECT id,name,date_out,performer_id FROM song";
    public static final String DELETE = "DELETE FROM song WHERE id=?";
    public static final String CREATE = "INSERT song (id, name, date_out, performer_id) VALUES (?, ?, ?, ?)";
    public static final String UPDATE = "UPDATE song SET name=?, date_out=?, performer_id=? WHERE id=?";

    public static final String FIND_BY_ID = "SELECT id,name,date_out,performer_id FROM song WHERE id=?";
    public static final String FIND_BY_NAME = "SELECT id,name,date_out,performer_id FROM song WHERE name=?";
    public static final String FIND_BY_DATE_OUT = "SELECT id,name,date_out,performer_id FROM song WHERE date_out=?";
    public static final String FIND_BY_PERFORMER_ID = "SELECT id,name,date_out,performer_id FROM song WHERE performer_id=?";

    public static final String DELETE_BY_NAME = "DELETE FROM song WHERE name=?";
    public static final String DELETE_BY_DATE_OUT = "DELETE FROM song WHERE date_out=?";
    public static final String DELETE_BY_PERFORMER_ID = "DELETE FROM song WHERE performer_id=?";

    private SongQuery() {
    }
}
