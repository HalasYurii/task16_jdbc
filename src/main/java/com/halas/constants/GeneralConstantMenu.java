package com.halas.constants;

public class GeneralConstantMenu {
    public static final String SQL_EXCEPTION =
            "\nSQL can't do current action, sorry..\nSQLException!\n\n";
    public static final String NULL_POINTER_EXCEPTION =
            "\nChoose correct choice please.\nNullPointerException!\n\n";

    public static final String SELECT_MSG =
            "Please, select menu point.";

    public static final String BACK_POINT = "0";
    public static final String EXIT_POINT = "0";
    public static final String MAIN_MENU = "\n***Main menu***";
    public static final String MENU_LOCATION = "\n***Menu Location***";
    public static final String MENU_MUSIC_DISC = "\n***Menu Music Disc***";
    public static final String MENU_PERFORMER = "\n***Menu Performer***";
    public static final String MENU_PUBLISHING_HOUSE = "\n***Menu Publishing house***";
    public static final String MENU_SONG = "\n***Menu Song***";
    public static final String MENU_FIND_BY_MUSIC_DISC = "\n***Menu music disc \'find by\'***";
    public static final String MENU_FIND_BY_LOCATION = "\n***Menu location \'find by\'***";
    public static final String MENU_FIND_BY_PERFORMER = "\n***Menu performer \'find by\'***";
    public static final String MENU_FIND_BY_PUBLISHING_HOUSE = "\n***Menu publishing house \'find by\'***";
    public static final String MENU_FIND_BY_SONG = "\n***Menu song \'find by\'***";
    public static final String MENU_DELETE_BY_MUSIC_DISC = "\n***Menu music disc \'delete by\'***";
    public static final String MENU_DELETE_BY_LOCATION = "\n***Menu location \'delete by\'***";
    public static final String MENU_DELETE_BY_PERFORMER = "\n***Menu performer \'delete by\'***";
    public static final String MENU_DELETE_BY_PUBLISHING_HOUSE = "\n***Menu publishing house \'delete by\'***";
    public static final String MENU_DELETE_BY_SONG = "\n***Menu song \'delete by\'***";


    private GeneralConstantMenu() {

    }
}
