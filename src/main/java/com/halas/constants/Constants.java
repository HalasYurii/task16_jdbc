package com.halas.constants;

import java.util.Scanner;

public class Constants {
    private static final String DELETE_DEFAULT_END = " for delete: ";
    private static final String FIND_DEFAULT_END = " by which you want get data: ";

    public static final String NAME_SCHEMA_BD = "NAME SCHEMA DATABASE: ";
    public static final String EMPTY_STRING = "";
    public static final String DEFAULT_DATE = "1000-10-10";

    public static final String TABLE_LOCATION = "Table: location";
    public static final String TABLE_MUSIC_DISC = "Table: music_disc";
    public static final String TABLE_PERFORMER = "Table: performer ";
    public static final String TABLE_PUBLISHING_HOUSE = "Table: publishing_house";
    public static final String TABLE_SONG = "Table: song";

    public static final String COUNT_CREATED_ROWS = "Count created rows: ";
    public static final String COUNT_UPDATED_ROWS = "Count updated rows: ";
    public static final String COUNT_DELETED_ROWS = "Count deleted rows: ";

    public static final String ID_INPUT = "Input ID: ";
    public static final String NAME_INPUT = "Input name: ";
    public static final String DATE_OUT_INPUT = "Input date out: ";
    public static final String PUB_HOUSE_ID_INPUT = "Input publishing house id: ";
    public static final String DATE_BORN_INPUT = "Input date born: ";
    public static final String COUNTRY_INPUT = "Input country: ";
    public static final String ADDRESS_INPUT = "Input address: ";
    public static final String TYPE_INPUT = "Input type disc: ";
    public static final String SURNAME_INPUT = "Input surname: ";
    public static final String MUSIC_DISC_ID_INPUT = "Input music disc id: ";
    public static final String PERFORMER_ID_INPUT = "Input performer id: ";

    public static final String INPUT_ID_FIND = "Input ID" + FIND_DEFAULT_END;
    public static final String INPUT_NAME_FIND = "Input name" + FIND_DEFAULT_END;
    public static final String INPUT_DATE_OUT_FIND = "Input date out" + FIND_DEFAULT_END;
    public static final String INPUT_PUBLISHING_HOUSE_ID_FIND = "Input publishing house id" + FIND_DEFAULT_END;
    public static final String INPUT_COUNTRY_FIND = "Input country" + FIND_DEFAULT_END;
    public static final String INPUT_ADDRESS_FIND = " Input address" + FIND_DEFAULT_END;
    public static final String INPUT_TYPE_FIND = "Input type of disc" + FIND_DEFAULT_END;
    public static final String INPUT_DATE_BORN_FIND = "Input date born" + FIND_DEFAULT_END;
    public static final String INPUT_SURNAME_FIND = "Input surname" + FIND_DEFAULT_END;
    public static final String INPUT_MUSIC_DISC_ID_FIND = "Input music disc id" + FIND_DEFAULT_END;
    public static final String INPUT_PERFORMER_ID_FIND = "Input performer id" + FIND_DEFAULT_END;

    public static final String INPUT_ID_DELETE = "Input ID" + DELETE_DEFAULT_END;
    public static final String INPUT_NAME_DELETE = "Input name" + DELETE_DEFAULT_END;
    public static final String INPUT_DATE_OUT_DELETE = "Input date out" + DELETE_DEFAULT_END;
    public static final String INPUT_PUBLISHING_HOUSE_ID_DELETE = "Input publishing house id" + DELETE_DEFAULT_END;
    public static final String INPUT_COUNTRY_DELETE = "Input country" + DELETE_DEFAULT_END;
    public static final String INPUT_ADDRESS_DELETE = " Input address" + DELETE_DEFAULT_END;
    public static final String INPUT_TYPE_DELETE = "Input type of disc" + DELETE_DEFAULT_END;
    public static final String INPUT_DATE_BORN_DELETE = "Input date born" + DELETE_DEFAULT_END;
    public static final String INPUT_SURNAME_DELETE = "Input surname" + DELETE_DEFAULT_END;
    public static final String INPUT_MUSIC_DISC_ID_DELETE = "Input music disc id" + DELETE_DEFAULT_END;
    public static final String INPUT_PERFORMER_ID_DELETE = "Input performer id" + DELETE_DEFAULT_END;

    public static final String CREATE_INFO = "****CREATE****";
    public static final String UPDATE_INFO = "****UPDATE****";
    public static final String DELETE_INFO = "****DELETE****";

    public static final String SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION =
            "Duplicate entry, ERROR SQLIntegrityConstraintViolationException.\n\n";

    public static final String BAD_ID_FIND = "Can't find by this id.\n\n";
    public static final String BAD_NAME_FIND = "Can't find by this name.\n\n";
    public static final String BAD_DATE_OUT_FIND = "Can't find by this date out.\n\n";
    public static final String BAD_PUBLISHING_HOUSE_ID_FIND = "Can't find by this publishing house id.\n\n";
    public static final String BAD_COUNTRY_FIND = "Can't find by this country.\n\n";
    public static final String BAD_ADDRESS_FIND = "Can't find by this address.\n\n";
    public static final String BAD_TYPE_FIND = "Can't find by this type.\n\n";
    public static final String BAD_DATE_BORN_FIND = "Can't find by this date born.\n\n";
    public static final String BAD_SURNAME_FIND = "Can't find by this surname.\n\n";
    public static final String BAD_MUSIC_DISC_ID_FIND = "Can't find by this music disc id.\n\n";
    public static final String BAD_PERFORMER_ID_FIND = "Can't find by this performer id.\n\n";

    public static final Scanner INPUT = new Scanner(System.in);

    private Constants() {
    }
}
