package com.halas.controller.chekers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.halas.constants.Constants.DEFAULT_DATE;

public class TakeInputtedData {

    private static final Logger LOG = LogManager.getLogger(TakeInputtedData.class);
    private static final Scanner INPUT = new Scanner(System.in);


    private TakeInputtedData() {
    }

    /***
     * if bad data then return -1
     * or return number
     */
    public static int getInt(String message) {
        int inputtedValue;
        try {
            LOG.info(message);
            String inputInt = INPUT.nextLine();
            inputtedValue = Integer.parseInt(inputInt);
        } catch (IllegalArgumentException e) {
            inputtedValue = -1;
        }
        return inputtedValue;
    }

    public static String getString(String message) {
        String inputtedText;
        LOG.info(message);
        inputtedText = INPUT.nextLine();
        return inputtedText;
    }


    private static boolean isInt(String symbolsToCheck) {
        Pattern p = Pattern.compile("^\\d+$");
        Matcher m = p.matcher(symbolsToCheck);
        if (m.find()) {
            return m.group().length() == symbolsToCheck.length();
        }
        return false;
    }


    private static String getStringDayFromString(String date) {
        return date.substring(date.length() - 2);
    }

    private static boolean isLessThanTwoLength(String value) {
        return value.length() < 2;
    }

    private static Integer getIntDayFromString(String date) {
        String day = getStringDayFromString(date);
        if (isLessThanTwoLength(date) || !isInt(day)) {
            return -1;
        }
        //String  value
        return Integer.parseInt(day);
    }

    /**
     * Increment day, coz Date from .sql
     * return -1 day.
     */
    private static String incrementDay(String date) {
        if (isLessThanTwoLength(date)) {
            return date;
        }
        String stringDay = getStringDayFromString(date);
        int dayInt = getIntDayFromString(stringDay);
        dayInt++;
        date = date.substring(0, date.length() - 2) + (dayInt);
        return date;
    }

    public static Date getDate(String message) {
        Date date;
        try {
            LOG.info(message);
            String inputtedDate = INPUT.nextLine();
            inputtedDate = incrementDay(inputtedDate);
            date = Date.valueOf(inputtedDate);
        } catch (IllegalArgumentException e) {
            date = Date.valueOf(DEFAULT_DATE);
        }
        return date;
    }
}