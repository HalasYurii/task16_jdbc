package com.halas.controller.interfaces;

import java.sql.SQLException;

public interface GeneralController {
    void showAll() throws SQLException;

    void showById() throws SQLException;

    void create() throws SQLException;

    void update() throws SQLException;

    void delete() throws SQLException;
}
