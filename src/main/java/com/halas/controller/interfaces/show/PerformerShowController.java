package com.halas.controller.interfaces.show;

import java.sql.SQLException;

public interface PerformerShowController {
    void showByName() throws SQLException;

    void showBySurname() throws SQLException;

    void showByDateBorn() throws SQLException;

    void showByMusicDiscID() throws SQLException;
}
