package com.halas.controller.interfaces.show;

import java.sql.SQLException;

public interface MusicDiscShowController {
    void showByName() throws SQLException;

    void showByType() throws SQLException;

    void showByDateOut() throws SQLException;

    void showByPublishingHouseID() throws SQLException;
}
