package com.halas.controller.interfaces.show;

import java.sql.SQLException;

public interface SongFindController {
    void showByName() throws SQLException;

    void showByDateOut() throws SQLException;

    void showByPerformerID() throws SQLException;
}
