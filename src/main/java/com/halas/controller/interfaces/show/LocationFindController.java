package com.halas.controller.interfaces.show;

import java.sql.SQLException;

public interface LocationFindController {
    void showByCountry() throws SQLException;

    void showByAddress() throws SQLException;

    void showByPublishingHouseID() throws SQLException;
}
