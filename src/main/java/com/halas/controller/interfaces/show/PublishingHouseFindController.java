package com.halas.controller.interfaces.show;

import java.sql.SQLException;

public interface PublishingHouseFindController {
    void showByName() throws SQLException;

    void showByDateBorn() throws SQLException;
}
