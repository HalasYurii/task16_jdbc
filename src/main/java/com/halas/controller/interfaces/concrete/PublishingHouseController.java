package com.halas.controller.interfaces.concrete;

import com.halas.controller.interfaces.GeneralController;
import com.halas.controller.interfaces.delete.PublishingHouseDeleteController;
import com.halas.controller.interfaces.show.PublishingHouseFindController;

public interface PublishingHouseController extends
        GeneralController,
        PublishingHouseFindController,
        PublishingHouseDeleteController {
}
