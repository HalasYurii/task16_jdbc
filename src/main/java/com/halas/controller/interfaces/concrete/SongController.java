package com.halas.controller.interfaces.concrete;

import com.halas.controller.interfaces.GeneralController;
import com.halas.controller.interfaces.delete.SongDeleteController;
import com.halas.controller.interfaces.show.SongFindController;

public interface SongController extends
        GeneralController,
        SongFindController,
        SongDeleteController {
}
