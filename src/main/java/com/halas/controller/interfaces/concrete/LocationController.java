package com.halas.controller.interfaces.concrete;

import com.halas.controller.interfaces.GeneralController;
import com.halas.controller.interfaces.delete.LocationDeleteController;
import com.halas.controller.interfaces.show.LocationFindController;

public interface LocationController extends
        GeneralController,
        LocationFindController,
        LocationDeleteController {
}
