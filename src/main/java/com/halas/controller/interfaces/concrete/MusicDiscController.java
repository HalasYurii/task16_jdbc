package com.halas.controller.interfaces.concrete;

import com.halas.controller.interfaces.GeneralController;
import com.halas.controller.interfaces.delete.MusicDiscDeleteController;
import com.halas.controller.interfaces.show.MusicDiscShowController;

public interface MusicDiscController extends
        GeneralController,
        MusicDiscShowController,
        MusicDiscDeleteController {

}
