package com.halas.controller.interfaces.concrete;

import com.halas.controller.interfaces.GeneralController;
import com.halas.controller.interfaces.delete.PerformerDeleteController;
import com.halas.controller.interfaces.show.PerformerShowController;

public interface PerformerController extends
        GeneralController,
        PerformerShowController,
        PerformerDeleteController {
}
