package com.halas.controller.interfaces.delete;

import java.sql.SQLException;

public interface PerformerDeleteController {
    void deleteByName() throws SQLException;

    void deleteBySurname() throws SQLException;

    void deleteByDateBorn() throws SQLException;

    void deleteByMusicDiscID() throws SQLException;
}
