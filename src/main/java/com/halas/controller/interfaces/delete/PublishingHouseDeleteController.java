package com.halas.controller.interfaces.delete;

import java.sql.SQLException;

public interface PublishingHouseDeleteController {
    void deleteByName() throws SQLException;

    void deleteByDateBorn() throws SQLException;
}
