package com.halas.controller.interfaces.delete;

import java.sql.SQLException;

public interface LocationDeleteController {
    void deleteByCountry() throws SQLException;

    void deleteByAddress() throws SQLException;

    void deleteByPublishingHouseID() throws SQLException;
}
