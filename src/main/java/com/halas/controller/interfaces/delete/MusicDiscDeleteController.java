package com.halas.controller.interfaces.delete;

import java.sql.SQLException;

public interface MusicDiscDeleteController {
    void deleteByName() throws SQLException;

    void deleteByType() throws SQLException;

    void deleteByDateOut() throws SQLException;

    void deleteByPublishingHouseID() throws SQLException;
}
