package com.halas.controller.interfaces.delete;

import java.sql.SQLException;

public interface SongDeleteController {
    void deleteByName() throws SQLException;

    void deleteByDateOut() throws SQLException;

    void deleteByPerformerID() throws SQLException;
}
