package com.halas.controller.implementation;

import com.halas.controller.interfaces.concrete.PublishingHouseController;
import com.halas.model.PublishingHouse;
import com.halas.service.implementation.PublishingHouseServiceImpl;
import com.halas.service.interfaces.concrete.PublishingHouseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.constants.Constants.*;
import static com.halas.controller.chekers.TakeInputtedData.*;

public class PublishingHouseControllerImpl implements PublishingHouseController {

    private Logger log;
    private PublishingHouseService publishingHouseService;

    public PublishingHouseControllerImpl() {
        log = LogManager.getLogger(PublishingHouseControllerImpl.class);
        publishingHouseService = new PublishingHouseServiceImpl();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        List<PublishingHouse> publishingHouses = publishingHouseService.findAll();
        publishingHouses.forEach(log::info);
        log.debug("\n");
    }

    @Override
    public void showById() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        Integer idFind = getInt(INPUT_ID_FIND);
        PublishingHouse publishingHouse = publishingHouseService.findById(idFind);
        if (Objects.isNull(publishingHouse)) {
            log.error(BAD_ID_FIND);
        } else {
            log.info(publishingHouse);
        }
    }

    @Override
    public void showByName() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        String nameFind = getString(INPUT_NAME_FIND);
        List<PublishingHouse> publishingHouse = publishingHouseService.findByName(nameFind);
        if (publishingHouse.isEmpty()) {
            log.error(BAD_NAME_FIND);
        } else {
            publishingHouse.forEach(log::info);
        }
    }

    @Override
    public void showByDateBorn() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        Date dateFind = getDate(INPUT_DATE_BORN_FIND);
        List<PublishingHouse> publishingHouse = publishingHouseService.findByDateBorn(dateFind);
        if (publishingHouse.isEmpty()) {
            log.error(BAD_DATE_BORN_FIND);
        } else {
            publishingHouse.forEach(log::info);
        }
    }

    private PublishingHouse setPublishingHouseByInputted() {
        PublishingHouse publishingHouse = new PublishingHouse();
        publishingHouse.setId(getInt(ID_INPUT));
        publishingHouse.setName(getString(NAME_INPUT));
        publishingHouse.setDateBorn(getDate(DATE_BORN_INPUT));
        return publishingHouse;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_PUBLISHING_HOUSE);
            log.info(CREATE_INFO);
            PublishingHouse publishingHouse = setPublishingHouseByInputted();
            int countRows = publishingHouseService.create(publishingHouse);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }

    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        log.info(UPDATE_INFO);
        PublishingHouse publishingHouse = setPublishingHouseByInputted();
        int countRows = publishingHouseService.update(publishingHouse);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void delete() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        log.info(DELETE_INFO);
        Integer idDelete = getInt(INPUT_ID_DELETE);
        int countRows = publishingHouseService.delete(idDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByName() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        log.info(DELETE_INFO);
        String nameDelete = getString(INPUT_ID_DELETE);
        int countRows = publishingHouseService.deleteByName(nameDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByDateBorn() throws SQLException {
        log.info(TABLE_PUBLISHING_HOUSE);
        log.info(DELETE_INFO);
        Date dateDelete = getDate(INPUT_DATE_BORN_DELETE);
        int countRows = publishingHouseService.deleteByDateBorn(dateDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
