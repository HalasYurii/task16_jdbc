package com.halas.controller.implementation;

import com.halas.controller.interfaces.concrete.PerformerController;
import com.halas.model.Performer;
import com.halas.service.implementation.PerformerServiceImpl;
import com.halas.service.interfaces.concrete.PerformerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.constants.Constants.*;
import static com.halas.controller.chekers.TakeInputtedData.*;

public class PerformerControllerImpl implements PerformerController {

    private Logger log;
    private PerformerService performerService;

    public PerformerControllerImpl() {
        log = LogManager.getLogger(PerformerControllerImpl.class);
        performerService = new PerformerServiceImpl();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_PERFORMER);
        List<Performer> performerList = performerService.findAll();
        performerList.forEach(log::info);
        log.debug("\n");
    }

    @Override
    public void showById() throws SQLException {
        log.info(TABLE_PERFORMER);
        Integer idFind = getInt(INPUT_ID_FIND);
        Performer performer = performerService.findById(idFind);
        if (Objects.isNull(performer)) {
            log.error(BAD_ID_FIND);
        } else {
            log.info(performer + "\n");
        }
    }


    @Override
    public void showByName() throws SQLException {
        log.info(TABLE_PERFORMER);
        String nameFind = getString(INPUT_NAME_FIND);
        List<Performer> performers = performerService.findByName(nameFind);
        if (performers.isEmpty()) {
            log.error(BAD_NAME_FIND);
        } else {
            performers.forEach(log::info);
        }
    }

    @Override
    public void showBySurname() throws SQLException {
        log.info(TABLE_PERFORMER);
        String surnameFind = getString(INPUT_SURNAME_FIND);
        List<Performer> performers = performerService.findBySurname(surnameFind);
        if (performers.isEmpty()) {
            log.error(BAD_SURNAME_FIND);
        } else {
            performers.forEach(log::info);
        }
    }

    @Override
    public void showByDateBorn() throws SQLException {
        log.info(TABLE_PERFORMER);
        Date dateFind = getDate(INPUT_DATE_BORN_FIND);
        List<Performer> performers = performerService.findByDateBorn(dateFind);
        if (performers.isEmpty()) {
            log.error(BAD_DATE_BORN_FIND);
        } else {
            performers.forEach(log::info);
        }
    }

    @Override
    public void showByMusicDiscID() throws SQLException {
        log.info(TABLE_PERFORMER);
        Integer discIdFind = getInt(INPUT_MUSIC_DISC_ID_FIND);
        List<Performer> performers = performerService.findByMusicDiscID(discIdFind);
        if (performers.isEmpty()) {
            log.error(BAD_MUSIC_DISC_ID_FIND);
        } else {
            performers.forEach(log::info);
        }
    }

    private Performer setPerformerByInputted() {
        Performer performer = new Performer();
        performer.setId(getInt(ID_INPUT));
        performer.setName(getString(NAME_INPUT));
        performer.setSurname(getString(SURNAME_INPUT));
        performer.setDateBorn(getDate(DATE_BORN_INPUT));
        performer.setMusicDiscId(getInt(MUSIC_DISC_ID_INPUT));
        return performer;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_PERFORMER);
            log.info(CREATE_INFO);
            Performer performer = setPerformerByInputted();
            int countRows = performerService.create(performer);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_PERFORMER);
        log.info(UPDATE_INFO);
        Performer performer = setPerformerByInputted();
        int countRows = performerService.update(performer);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void delete() throws SQLException {
        log.info(TABLE_PERFORMER);
        log.info(DELETE_INFO);
        Integer idDelete = getInt(INPUT_ID_DELETE);
        int countRows = performerService.delete(idDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByName() throws SQLException {
        log.info(TABLE_PERFORMER);
        log.info(DELETE_INFO);
        String nameDelete = getString(INPUT_NAME_DELETE);
        int countRows = performerService.deleteByName(nameDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteBySurname() throws SQLException {
        log.info(TABLE_PERFORMER);
        log.info(DELETE_INFO);
        String surnameDelete = getString(INPUT_SURNAME_DELETE);
        int countRows = performerService.deleteBySurname(surnameDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByDateBorn() throws SQLException {
        log.info(TABLE_PERFORMER);
        log.info(DELETE_INFO);
        Date dateDelete = getDate(INPUT_DATE_BORN_DELETE);
        int countRows = performerService.deleteByDateBorn(dateDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByMusicDiscID() throws SQLException {
        log.info(TABLE_PERFORMER);
        log.info(DELETE_INFO);
        Integer discDelete = getInt(INPUT_MUSIC_DISC_ID_DELETE);
        int countRows = performerService.deleteByMusicDiscID(discDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
