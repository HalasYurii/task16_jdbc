package com.halas.controller.implementation;

import com.halas.controller.interfaces.concrete.MusicDiscController;
import com.halas.model.MusicDisc;
import com.halas.service.implementation.MusicDiscServiceImpl;
import com.halas.service.interfaces.concrete.MusicDiscService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.constants.Constants.*;
import static com.halas.controller.chekers.TakeInputtedData.*;

public class MusicDiscControllerImpl implements MusicDiscController {
    private Logger log;
    private MusicDiscService musicDiscService;

    public MusicDiscControllerImpl() {
        log = LogManager.getLogger(MusicDiscControllerImpl.class);
        musicDiscService = new MusicDiscServiceImpl();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        List<MusicDisc> musicDiscs = musicDiscService.findAll();
        musicDiscs.forEach(log::info);
        log.debug("\n");
    }

    @Override
    public void showById() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        Integer idFind = getInt(INPUT_ID_FIND);
        MusicDisc musicDisc = musicDiscService.findById(idFind);
        if (Objects.isNull(musicDisc)) {
            log.error(BAD_ID_FIND);
        } else {
            log.info(musicDisc + "\n");
        }
    }

    @Override
    public void showByName() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        String nameFind = getString(INPUT_NAME_FIND);
        List<MusicDisc> musicDiscs = musicDiscService.findByName(nameFind);
        if (Objects.isNull(musicDiscs)) {
            log.error(BAD_NAME_FIND);
        } else {
            musicDiscs.forEach(log::info);
        }
    }

    @Override
    public void showByType() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        String typeFind = getString(INPUT_TYPE_FIND);
        List<MusicDisc> musicDiscs = musicDiscService.findByType(typeFind);
        if (musicDiscs.isEmpty()) {
            log.error(BAD_TYPE_FIND);
        } else {
            musicDiscs.forEach(log::info);
        }
    }

    @Override
    public void showByDateOut() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        Date dateFind = getDate(INPUT_DATE_OUT_FIND);
        List<MusicDisc> musicDiscs = musicDiscService.findByDateOut(dateFind);
        if (musicDiscs.isEmpty()) {
            log.error(BAD_DATE_OUT_FIND);
        } else {
            musicDiscs.forEach(log::info);
        }
    }

    @Override
    public void showByPublishingHouseID() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        Integer idFind = getInt(INPUT_PUBLISHING_HOUSE_ID_FIND);
        List<MusicDisc> musicDiscs = musicDiscService.findByPublishingHouseID(idFind);
        if (musicDiscs.isEmpty()) {
            log.error(BAD_PUBLISHING_HOUSE_ID_FIND);
        } else {
            musicDiscs.forEach(log::info);
        }
    }

    private MusicDisc setMusicDiscByInputted() {
        MusicDisc musicDisc = new MusicDisc();
        musicDisc.setId(getInt(ID_INPUT));
        musicDisc.setName(getString(NAME_INPUT));
        musicDisc.setType(getString(TYPE_INPUT));
        musicDisc.setDateOut(getDate(DATE_OUT_INPUT));
        musicDisc.setPublishingHouseId(getInt(PUB_HOUSE_ID_INPUT));
        return musicDisc;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_MUSIC_DISC);
            log.info(CREATE_INFO);
            MusicDisc musicDisc = setMusicDiscByInputted();
            int countRows = musicDiscService.create(musicDisc);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        log.info(UPDATE_INFO);
        MusicDisc musicDisc = setMusicDiscByInputted();
        int countRows = musicDiscService.update(musicDisc);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void delete() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        log.info(DELETE_INFO);
        Integer idDelete = getInt(INPUT_ID_DELETE);
        int countRows = musicDiscService.delete(idDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByName() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        log.info(DELETE_INFO);
        String nameDelete = getString(INPUT_NAME_DELETE);
        int countRows = musicDiscService.deleteByName(nameDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByType() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        log.info(DELETE_INFO);
        String typeDelete = getString(INPUT_TYPE_DELETE);
        int countRows = musicDiscService.deleteByType(typeDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByDateOut() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        log.info(DELETE_INFO);
        Date dateOutDelete = getDate(INPUT_DATE_OUT_DELETE);
        int countRows = musicDiscService.deleteByDateOut(dateOutDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPublishingHouseID() throws SQLException {
        log.info(TABLE_MUSIC_DISC);
        log.info(DELETE_INFO);
        Integer publishingDelete = getInt(INPUT_PUBLISHING_HOUSE_ID_DELETE);
        int countRows = musicDiscService.deleteByPublishingHouseID(publishingDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
