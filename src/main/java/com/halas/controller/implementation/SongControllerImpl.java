package com.halas.controller.implementation;

import com.halas.controller.interfaces.concrete.SongController;
import com.halas.model.Song;
import com.halas.service.implementation.SongServiceImpl;
import com.halas.service.interfaces.concrete.SongService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.constants.Constants.*;
import static com.halas.controller.chekers.TakeInputtedData.*;

public class SongControllerImpl implements SongController {
    private Logger log;
    private SongService songService;

    public SongControllerImpl() {
        log = LogManager.getLogger(SongControllerImpl.class);
        songService = new SongServiceImpl();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_SONG);
        List<Song> songList = songService.findAll();
        songList.forEach(log::info);
        log.debug("\n");
    }

    @Override
    public void showById() throws SQLException {
        log.info(TABLE_SONG);
        Integer idFind = getInt(INPUT_ID_FIND);
        Song song = songService.findById(idFind);
        if (Objects.isNull(song)) {
            log.error(BAD_ID_FIND);
        } else {
            log.info(song);
        }
    }

    @Override
    public void showByName() throws SQLException {
        log.info(TABLE_SONG);
        String nameFind = getString(INPUT_NAME_FIND);
        List<Song> songs = songService.findByName(nameFind);
        if (songs.isEmpty()) {
            log.error(BAD_NAME_FIND);
        } else {
            songs.forEach(log::info);
        }
    }

    @Override
    public void showByDateOut() throws SQLException {
        log.info(TABLE_SONG);
        Date dateFind = getDate(INPUT_DATE_OUT_FIND);
        List<Song> songs = songService.findByDateOut(dateFind);
        if (songs.isEmpty()) {
            log.error(BAD_DATE_OUT_FIND);
        } else {
            songs.forEach(log::info);
        }
    }

    @Override
    public void showByPerformerID() throws SQLException {
        log.info(TABLE_SONG);
        Integer performerIdFind = getInt(INPUT_PERFORMER_ID_FIND);
        List<Song> songs = songService.findByPerformerID(performerIdFind);
        if (songs.isEmpty()) {
            log.error(BAD_PERFORMER_ID_FIND);
        } else {
            songs.forEach(log::info);
        }
    }

    private Song setSongByInputted() {
        Song song = new Song();
        song.setId(getInt(ID_INPUT));
        song.setName(getString(NAME_INPUT));
        song.setDateOut(getDate(DATE_OUT_INPUT));
        song.setPerformerId(getInt(PERFORMER_ID_INPUT));
        return song;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_SONG);
            log.info(CREATE_INFO);
            Song song = setSongByInputted();
            int countRows = songService.create(song);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_SONG);
        log.info(UPDATE_INFO);
        Song song = setSongByInputted();
        int countRows = songService.update(song);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void delete() throws SQLException {
        log.info(TABLE_SONG);
        log.info(DELETE_INFO);
        Integer idDelete = getInt(INPUT_ID_DELETE);
        int countRows = songService.delete(idDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByName() throws SQLException {
        log.info(TABLE_SONG);
        log.info(DELETE_INFO);
        String nameDelete = getString(INPUT_NAME_DELETE);
        int countRows = songService.deleteByName(nameDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByDateOut() throws SQLException {
        log.info(TABLE_SONG);
        log.info(DELETE_INFO);
        Date dateDelete = getDate(INPUT_DATE_OUT_DELETE);
        int countRows = songService.deleteByDateOut(dateDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPerformerID() throws SQLException {
        log.info(TABLE_SONG);
        log.info(DELETE_INFO);
        Integer performerIdDelete = getInt(INPUT_PERFORMER_ID_DELETE);
        int countRows = songService.deleteByPerformerID(performerIdDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}