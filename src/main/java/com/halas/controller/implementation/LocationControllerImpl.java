package com.halas.controller.implementation;

import com.halas.controller.interfaces.concrete.LocationController;
import com.halas.model.Location;
import com.halas.service.implementation.LocationServiceImpl;
import com.halas.service.interfaces.concrete.LocationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.constants.Constants.*;
import static com.halas.controller.chekers.TakeInputtedData.getInt;
import static com.halas.controller.chekers.TakeInputtedData.getString;

public class LocationControllerImpl implements LocationController {
    private Logger log;
    private LocationService locationService;

    public LocationControllerImpl() {
        log = LogManager.getLogger(LocationControllerImpl.class);
        locationService = new LocationServiceImpl();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_LOCATION);
        List<Location> locations = locationService.findAll();
        locations.forEach(log::info);
        log.debug("\n");
    }

    @Override
    public void showById() throws SQLException {
        log.info(TABLE_LOCATION);
        Integer idFind = getInt(INPUT_ID_FIND);
        Location location = locationService.findById(idFind);
        if (Objects.isNull(location)) {
            log.error(BAD_ID_FIND);
        } else {
            log.info(location + "\n");
        }
    }

    @Override
    public void showByCountry() throws SQLException {
        log.info(TABLE_LOCATION);
        String country = getString(INPUT_COUNTRY_FIND);
        List<Location> locations = locationService.findByCountry(country);
        if (locations.isEmpty()) {
            log.error(BAD_COUNTRY_FIND);
        } else {
            locations.forEach(log::info);
        }
    }

    @Override
    public void showByAddress() throws SQLException {
        log.info(TABLE_LOCATION);
        String address = getString(INPUT_ADDRESS_FIND);
        List<Location> locations = locationService.findByAddress(address);
        if (locations.isEmpty()) {
            log.error(BAD_ADDRESS_FIND);
        } else {
            locations.forEach(log::info);
        }
    }

    @Override
    public void showByPublishingHouseID() throws SQLException {
        log.info(TABLE_LOCATION);
        Integer idPublishingFind = getInt(INPUT_PUBLISHING_HOUSE_ID_FIND);
        List<Location> locations = locationService.findByPublishingHouseID(idPublishingFind);
        if (locations.isEmpty()) {
            log.error(BAD_PUBLISHING_HOUSE_ID_FIND);
        } else {
            locations.forEach(log::info);
        }
    }

    private Location setLocationByInputted() {
        Location location = new Location();
        location.setId(getInt(ID_INPUT));
        location.setCountry(getString(COUNTRY_INPUT));
        location.setAddress(getString(ADDRESS_INPUT));
        location.setPublishingHouseId(getInt(PUB_HOUSE_ID_INPUT));
        return location;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_LOCATION);
            log.info(CREATE_INFO);
            Location location = setLocationByInputted();
            int countRows = locationService.create(location);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_LOCATION);
        log.info(UPDATE_INFO);
        Location location = setLocationByInputted();
        int countRows = locationService.update(location);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void delete() throws SQLException {
        log.info(TABLE_LOCATION);
        log.info(DELETE_INFO);
        Integer idDelete = getInt(INPUT_ID_DELETE);
        int countRows = locationService.delete(idDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByCountry() throws SQLException {
        log.info(TABLE_LOCATION);
        log.info(DELETE_INFO);
        String countryDelete = getString(INPUT_COUNTRY_DELETE);
        int countRows = locationService.deleteByCountry(countryDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByAddress() throws SQLException {
        log.info(TABLE_LOCATION);
        log.info(DELETE_INFO);
        String addressDelete = getString(INPUT_ADDRESS_DELETE);
        int countRows = locationService.deleteByAddress(addressDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPublishingHouseID() throws SQLException {
        log.info(TABLE_LOCATION);
        log.info(DELETE_INFO);
        Integer publishingHouseDelete = getInt(INPUT_PUBLISHING_HOUSE_ID_DELETE);
        int countRows = locationService.deleteByPublishingHouseID(publishingHouseDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}