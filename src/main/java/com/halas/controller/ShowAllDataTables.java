package com.halas.controller;

import com.halas.controller.implementation.*;

import java.sql.SQLException;

public class ShowAllDataTables {
    public void show() throws SQLException {
        new LocationControllerImpl().showAll();
        new MusicDiscControllerImpl().showAll();
        new PerformerControllerImpl().showAll();
        new PublishingHouseControllerImpl().showAll();
        new SongControllerImpl().showAll();
    }
}
